#!/usr/bin/python
# Copyright (C) 2009 Julian Andres Klode <jak@debian.org>
#
# Released under the terms of the GNU General Publice License, version 3
# or (at your option) any later version. See COPYING for details.
#
"""Example to demonstrate the power of debimg's repository management.

This example creates a directory example.debimg, with two subdirectories:
    - pool: This holds debimg's file pool (see debimg.core.files.Pool)
    - repo: This holds the created repository (debimg.core.repository)

It creates a repository of all required packages, using your local apt sources.
"""
from __future__ import with_statement
import optparse
import os
import sys

from debimg.core.resolver import Resolver
from debimg.core.files import Pool
from debimg.core.repository import Repository


def main(args):
    """Called when the script is executed, 'args' should be sys.argv[1:]."""
    parser = optparse.OptionParser()
    parser.add_option('-u', '--user', help='Same as in GPG')
    parser.add_option('-d', '--dir', default='example.debimg',
                      help='location to store files into (default: %default)')
    parser.add_option('-c', '--contents', action="store_true",
                     help='Create Contents-*.gz files')
    opts, args = parser.parse_args(args)

    # Create a pool, which manages the access to the files.
    pool = Pool(os.path.join(opts.dir, 'pool'))

    # Create a resolver using your local apt configuration.
    pkgs = Resolver()

    # Create a new repository.
    repo = Repository(pool, os.path.join(opts.dir, 'repo'))

    # Create a new distribution and store it in dist.
    dist = repo.add_distribution('lenny')

    # Add all packages with priority required to the resolver.
    pkgs.add_priority('required')

    # Add the packages from the resolver to the distribution. We are using the
    # concept of package groups, which defines a set of packages which depend
    # on each other.
    for group in pkgs.groups():
        dist.add_group(group)

    pool.compact()   # Optional step, merge files with same SHA1
    pool.fetch()     # Fetch all the packages from the mirror
    repo.finalize_files() # Link the files into the repository
    repo.finalize_packages() # Create Packages files (uncompressed and gzip)

    # If the option -c,--contents has been given, this creates the
    # Contents-ARCH.gz files in dists/CODENAME/.
    if opts.contents:
        repo.finalize_contents()

    # Now create our Release files, and sign them using the key provided on
    # the commandline. If no key has been provided, do not sign it.
    dist.finalize_release(suite='stable', version='5.0.0', key=opts.user)

    # Now we want to create the various MD5SUMS, SHA1SUMS and SHA256SUMS files
    # for all the files inside the repository (except those files themselves).
    repo.finalize_hashsums('md5')
    repo.finalize_hashsums('sha1')
    repo.finalize_hashsums('sha256')

if __name__ == '__main__':
    main(sys.argv[1:])
