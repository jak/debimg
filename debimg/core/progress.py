# Copyright (C) 2009 Julian Andres Klode <jak@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable-msg=C0103,R0201,W0613
#
"""Progress classes for use with apt functions.

These classes implement the same interfaces as the ones in `apt.progress`, but
use the `logging` module instead. They also have less output, and only report
when an action is done and not the progress.
"""

import logging


class LoggingOpProgress(object):
    """A simple text based cache open reporting class."""

    def __init__(self):
        self.subOp = None

    def done(self):
        """Will be called automatically once a sub-operation is done."""
        if self.subOp:
            logging.debug('Opening cache: %s is done' % self.subOp)

    def update(self, percent):
        """Will be called automatically from time to time."""

    def __del__(self):
        logging.info('Cache is initialized')


class LoggingFetchProgress(object):
    """A class for the fetching output in terminals, files, etc.

    In contrast to `apt.progress.TextFetchProgress`, this class does not show
    any progress except when the state changes (eg. when the download is done).
    """

    _status = 'Fetched', 'Queued', 'Failed', 'Hit', 'Ignored'

    def pulse(self):
        """Executed sometimes during the download."""
        return True

    def start(self):
        """Executed when the download starts."""
        logging.info('Starting downloads')

    def stop(self):
        """Executed when all downloads are completed."""
        logging.info('Finished downloads')

    def updateStatus(self, uri, descr, short_descr, status):
        """Update the status of the downloads."""
        if self._status[status] == 'Fetched':
            logging.info("%-7s %s" % (self._status[status], descr))
        elif self._status[status] != 'Queued':
            logging.debug("%-7s %s" % (self._status[status], descr))
