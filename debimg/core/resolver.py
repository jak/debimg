# Copyright (C) 2009 Julian Andres Klode <jak@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable-msg=R0902
"""Simple Dependency Resolver.

This module provides the Package and Resolver classes, which together form
the dependency resolver of debimg.
"""

from __future__ import with_statement
from contextlib import contextmanager
from operator import attrgetter
import logging
import os.path

from debimg.core.progress import LoggingFetchProgress, LoggingOpProgress
import apt_pkg


PRIO = dict(important=apt_pkg.PriImportant, required=apt_pkg.PriRequired,
            standard=apt_pkg.PriStandard, optional=apt_pkg.PriOptional,
            extra=apt_pkg.PriExtra)

__all__ = ['Package', 'Resolver']


class RecordsLockingError(Exception):
    """Raised when the locking fails."""


class Records(object): # pylint: disable-msg=R0903
    """Records which can be locked."""

    __slots__ = ('records', 'locked')

    def __init__(self, cache):
        self.records = apt_pkg.GetPkgRecords(cache)
        self.locked = False

    def lookup(self, veriter):
        """Lookup a record given by veriter."""
        if self.locked:
            raise RecordsLockingError("Locked.")
        return self.records.Lookup(veriter)


class Package(object):
    """Information about a package.

    Please note that the attribute 'arch' is normally not all, this only
    happens when the package is in fact "all", and no parameter 'arch' is
    given.

    The resolver only creates package objects using the value defined in
    APT::Architecture at the time the resolver was created.
    """

    __slots__ = ('_records', '_record', '_list', '_cand',
                 'group', 'name', 'done', 'size', 'arch', '_looked_up')

    def __init__(self, records, list_, cand, arch=None):
        self._cand = cand
        self.name = cand.ParentPkg.Name
        self.arch = arch or cand.Arch
        self.done = 0
        self.size = cand.Size
        self._records = records
        self._record = None
        self._list = list_
        self._looked_up = False
        self.group = set([self])

    def __hash__(self):
        return self._cand.Hash

    def __repr__(self):
        return '<Package object: name:"%s" arch:"%s" done:%d>' % (
                self.name, self.arch, self.done)

    def add_cyclic(self, other):
        """Add another cyclic dependency.

        The parameter `other` specifies another `Package()` object, which has
        a cyclic dependency to this package, ie. they require each other.
        """
        self.group.update(other.group)
        for package in other.group:
            package.group = self.group

    @property
    def fullname(self):
        """Return the fullname of the package, ie. [component/]section/name."""
        return self._cand.Section + '/' + self.name

    @property
    def component(self):
        """Return the component of the package.

        The component is normally one of "main", "contrib", "non-free".
        """
        base, sep, _ = self._cand.Section.partition('/')
        return base if sep else 'main'

    @property
    def uri(self):
        """Return the URI of the package."""
        indexfile = self._list.FindIndex(self._cand.FileList[0][0])
        with self.record() as record:
            return indexfile.ArchiveURI(record.FileName)

    @contextmanager
    def record(self):
        """Context manager for looking up a record.

        This can be used to speedup multiple lookups on the same records,
        being more effective. It also makes it impossible to lookup a second
        package while being inside another record context.

        See the documentation of apt_pkg for how to work with the resulting
        pkgRecords object (please do not use its Lookup method).

        Example:
            with package.record() as record:
                print record.FileName
        """
        if self._looked_up: # Already inside a lookup() context.
            yield self._records.records
        else:
            self._records.lookup(self._cand.FileList[0])
            try:
                self._looked_up = True
                self._records.locked = True # Lock
                yield self._records.records
            finally:
                self._records.locked = False # Unlock
                self._looked_up = False


class Resolver(object):
    """Simple Dependency Resolver.

    Resolves dependencies of packages, and provides access to a sorted
    sequence of Package objects, using the groups property.

    During dependency resolving, some aspects of the debian package managment
    are ignored. This includes conflicts, and also partially versioned
    dependencies. Furthermore, Dir::State::status is not set, which means the
    current state on the system is ignored.

    Several parameters are supported by this class.

    The parameter 'rootdir' allows to use the given directory as its '/',
    storing all files in the standard locations below it.

    The parameter 'sources' takes a string containing one or more lines which
    will be written into 'rootdir'/etc/apt/sources.list. Possibly existing
    content will be replaced.

    The parameter 'update' is a boolean value determing whether the package
    lists will be refreshed. If it is False (the default), the package lists
    will only be updated if the current one is empty.
    """

    __slots__ = ('_cache', '_depcache', '_records', '_list', '_arch', #apt
                 '_deps', '_tasks', '_depscnt', # dependencies
                 '_benchmarking') # others

    _benchmarking = False

    def __init__(self, rootdir='/', sources=None, update=False):
        apt_pkg.Config.Set('Dir', os.path.abspath(rootdir))
        apt_pkg.Config.Set('Dir::State::status', '/dev/null')
        # Create the directories we need
        for path in ('/etc/apt', '/var/cache/apt/archives/partial',
                     '/var/lib/apt/lists/partial'):
            if not os.path.exists(rootdir + path):
                os.makedirs(rootdir + path)
        # Do we have some sources given
        if sources:
            with open(rootdir + '/etc/apt/sources.list', 'w') as fobj:
                fobj.write(sources)
        # Configuration is done, initialize the cache
        self._deps = dict()
        self._depscnt = 0
        self._arch = apt_pkg.Config['APT::Architecture']
        self._cache = apt_pkg.GetCache(LoggingOpProgress())
        self._list = apt_pkg.GetPkgSourceList()
        self._list.ReadMainList()
        # Update the cache if requested or needed.
        if update or not self._cache.Packages:
            self._cache.Update(LoggingFetchProgress(), self._list)
            self._cache.Open(LoggingOpProgress())
        self._depcache = apt_pkg.GetDepCache(self._cache)
        self._records = Records(self._cache)
        # The cache for tasks. Will be filled on the addition of a task.
        self._tasks = {'key': {}, 'task-fields': {}}

    def groups(self):
        """Iterator over package groups.

        A package group is an iterator of Package objects which depend on each
        other.

        It is currently implemented using the 'set()' type, but this is an
        implementation detail and you should not depend on a package group
        being a 'set()' object.
        """
        for dep in sorted(self._deps.itervalues(), key=attrgetter('done')):
            # Use the last possible group ((X1,A,X2) and X1==X2 => X=X2)
            if all(subdep.done <= dep.done for subdep in dep.group):
                yield dep.group

    def no_group(self):
        """Iterator over all packages not in a group."""
        for package in self:
            if not package.done:
                yield package

    def __getitem__(self, pkgname):
        """Return a Package() object for the given package.

        You should not depend on this interface. The returned Package() object
        is not complete, it contains no dependency information, etc. This
        method exists for making testing easier.
        """
        try:
            return self._deps[pkgname]
        except KeyError:
            return Package(self._records, self._list,
                        self._depcache.GetCandidateVer(self._cache[pkgname]),
                           self._arch)

    def __iter__(self):
        """Iterator interface for Packages.

        You should not depend on this functionality, it may go away, or be
        replaced by a groups iterator.
        """
        for pkg in self._cache.Packages:
            try:
                yield self._deps[pkg.Name]
            except KeyError:
                cand = self._depcache.GetCandidateVer(pkg)
                if cand is not None:
                    yield Package(self._records, self._list, cand, self._arch)

    def _add_pkg(self, pkg, cand):
        """Internal function: Resolve the dependencies of a package 'pkg'."""
        if pkg.Name in self._deps:
            return True

        self._deps[pkg.Name] = Package(self._records, self._list, cand,
                                       self._arch)

        get = cand.DependsList.get

        for dep_or in (get('PreDepends') or []) + (get('Depends') or []):
            to_add, satisfied, provider = None, False, None
            for dep in dep_or:
                tgts = dep.AllTargets()
                for dep_cand in tgts:
                    dep_pkg = dep_cand.ParentPkg
                    if dep_pkg.Name in self._deps:
                        dep_info = self._deps[dep_pkg.Name]
                        if not dep_info.done:
                            self._deps[pkg.Name].add_cyclic(dep_info)
                        satisfied = True
                        break
                    if to_add or 'debian-installer' in dep_cand.Section:
                        continue
                    if ((dep_pkg.Name == dep.TargetPkg.Name) or
                          len(tgts) == 1):
                        to_add = dep_cand
                    provider = dep_cand
                if satisfied:
                    break
                if provider and not to_add:
                    # If dep-or is a | b, and the dependency b is not satisfied
                    # already, and a is a virtual package, and b a real one,
                    # select a (the virtual one).
                    to_add = provider

            to_add = (to_add or provider)

            if not satisfied and not (to_add and self._add_pkg(
                                      to_add.ParentPkg, to_add)):
                if not self._benchmarking:
                    logging.warning('Could not install %s (depends on the '
                                    'uninstallable package %s)' % (pkg.Name,
                             ' | '.join(dep.TargetPkg.Name for dep in dep_or)))
                del self._deps[pkg.Name]
                return
            elif not satisfied:
                # Partially fix the problem with (a=>b=>c=>a) dependencies.
                tgt = self._deps[to_add.ParentPkg.Name]
                if 0 in (pkgx.done for pkgx in tgt.group):
                    self._deps[pkg.Name].add_cyclic(tgt)

        self._deps[pkg.Name].done = self._depscnt = self._depscnt + 1
        return True

    def _get_record(self, cand):
        """Get the record for the specified candidate."""
        if not self._records.lookup(cand.FileList[0]):
            raise KeyError('There is no lookup for %s' % cand)
        return apt_pkg.ParseSection(self._records.records.Record)

    def add_package(self, pkgname):
        """Add a package, specified by 'pkgname'."""
        logging.info('Including package %s' % pkgname)
        pkg = self._cache[pkgname]
        cand = self._depcache.GetCandidateVer(pkg)
        if cand is not None:
            self._add_pkg(pkg, cand)

    def add_priority(self, priority):
        """Add packages based on their priority."""
        logging.info('Including priority %s' % priority)
        # The integer is faster to compare here.
        priority_int = PRIO[priority]
        for pkg in self._cache.Packages:
            cand = self._depcache.GetCandidateVer(pkg)
            if cand is not None and priority_int == cand.Priority:
                self._add_pkg(pkg, cand)

    ############## TASK Handling #####################################

    def _load_tasksel(self):
        """Load the task data from tasksel."""
        with open('/usr/share/tasksel/debian-tasks.desc') as fobj:
            parser = apt_pkg.ParseTagFile(fobj)
            while parser.Step() == 1:
                try:
                    task, key = parser.Section['Task'], parser.Section['Key']
                except KeyError:
                    continue
                self._tasks['key'][task] = []
                for pkgname in key.split():
                    try:
                        pkg = self._cache[pkgname]
                        cand = self._depcache.GetCandidateVer(pkg)
                        self._tasks['key'][task].append((pkg, cand))
                    except KeyError:
                        logging.warning('Ignoring key package %s of task %s' %
                                        (pkgname, task))

    def _load_tasks(self):
        """Iterate through the packages, lookup their records and check
        if they are in the right task."""
        for pkg in self._cache.Packages:
            cand = self._depcache.GetCandidateVer(pkg)
            try:
                tasks = self._get_record(cand)['Task']
            except (AttributeError, KeyError):
                continue
            # Cache the results, speedups second runs
            for task in tasks.split(', '):
                try:
                    self._tasks['task-fields'][task].append((pkg, cand))
                except KeyError:
                    self._tasks['task-fields'][task] = [(pkg, cand)]

    def add_tasks(self, tasks):
        """Add tasks, in the correct order.

        The parameter 'tasks' specifies an iterable, with the name of the
        to-be-added tasks. First of all, all "key packages" are included, and
        afterwards all remaining packages.

        The task "standard" refers to all packages with "standard" priority. To
        ease the use of debimg, we simply call add_priority("standard") when
        this task is specified.
        """

        logging.info('Including tasks: %s' % ','.join(tasks))

        if not (self._tasks['key'] or self._tasks['task-fields']):
            try:
                self._load_tasksel()
            except OSError:
                logging.warning('Could not load tasksel data!')
            self._load_tasks()

        for task in tasks:
            if task == 'standard':
                self.add_priority('standard')
                continue
            for pkg, cand in self._tasks['key'][task]:
                self._add_pkg(pkg, cand)

        for task in tasks:
            if task == 'standard':
                continue
            for pkg, cand in self._tasks['task-fields'][task]:
                self._add_pkg(pkg, cand)

    ############## END TASK Handling #####################################
