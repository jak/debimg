# Copyright (C) 2009 Julian Andres Klode <jak@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""File-related functionality, especially File and Pool classes.

The classes in these module provide an abstraction layer to access files from
various locations using a single interface.

The interface should not be considered really stable yet. This is just an
initial version to allow us to go forward. If we realize at some point that
we need something else, we will change.

This module still needs some improvements. Many methods are called multiple
times although we could cache the value instead.
"""
from __future__ import with_statement
import hashlib
import logging
import os
import sys

import apt_pkg
import pycurl

__all__ = ('File', 'HashingWriter', 'Pool')


class File(object):
    """Represent a single file in the pool.

    The parameter 'tdir' defines the target directory, where the file will
    be stored at (if it needs to be fetched). It usually refers to the
    directory of the pool.

    The parameter 'source' specifies a location at which the file can be
    accessed. This supports file:/, copy:/, http:/ and ftp:/ urls.

    The parameters 'size', 'md5', 'sha1' give default values for the
    corresponding functions. They may be removed in a future release.

    The parameter 'pkg' can be used to specify a debimg.core.resolver.Package()
    object, from which information about size, md5, sha1, sha256 will be taken.
    """

    # pylint: disable-msg=R0913,R0902
    __slots__ = ('sources', '_size', '_local', '_sha256', '_sha1',
                 '_md5', 'path')

    def __init__(self, tdir, source, size=None, md5=None, sha1=None,
                 sha256=None):
        """See the class documentation"""
        self.path = tdir
        if not ':/' in source:
            source = 'file:' + os.path.abspath(source)
        self.sources = set([source])
        self._local = None
        self._size = size
        self._md5, self._sha1, self._sha256 = md5, sha1, sha256

    def size(self):
        """Return the size of the file."""
        if self._size is None:
            self._size = os.path.getsize(self.local())
        return self._size

    @property
    def is_local(self):
        """Return whether the file is locally available."""
        if self._local is not None:
            return True
        uri = self.uri
        return uri.startswith('file:/') or uri.startswith('copy:/')

    @property
    def uri(self):
        """Return a source url, prefer file:/, copy:/, http:/, ftp:/."""
        if self._local:
            return self._local
        copy, remote = None, None
        for uri in self.sources:
            if uri.startswith('file:/'):
                self._local = uri
                return uri
            if not copy and uri.startswith('copy:/'):
                self._local = uri
                copy = uri
            if not remote:
                remote = uri
        return copy or remote

    def __repr__(self):
        """Short representation of the object, including its uri."""
        return '<File: uri:"%s">' % self.uri

    def fetch(self, curl=None):
        """Fetch the file return HashingWriter() object."""
        if curl is None:
            my_curl = pycurl.Curl()
            my_curl.setopt(pycurl.FOLLOWLOCATION, 1)
        else:
            my_curl = curl
        with HashingWriter(self, fobj=self) as writing:
            uri = self.uri
            my_curl.setopt(pycurl.URL, uri)
            my_curl.setopt(pycurl.WRITEFUNCTION, writing.write)
            my_curl.perform()
            if my_curl.getinfo(pycurl.HTTP_CODE) == 200:
                logging.debug("Fetched '%s'" % uri)
            else:
                logging.error("Could not fetch '%s' (Errno: %s)" %
                              (uri, my_curl.getinfo(pycurl.HTTP_CODE)))
        if curl is None:
            my_curl.close()
        return writing

    def local(self, fetch=True):
        """Return the path to the local file, fetch if needed."""
        if self._local:
            return self._local.lstrip('file:').lstrip('copy:')
        elif self.is_local:
            return self.uri.lstrip('file:').lstrip('copy:')
        elif fetch:
            print 'I: Fetching', self.uri, '...',
            sys.stdout.flush()
            self.fetch()
            print 'DONE'
            return self._local.lstrip('file:').lstrip('copy:')
        else:
            local = self._sha1 or apt_pkg.URItoFileName(self.uri)
            return os.path.join(self.path, local)

    def open(self):
        """Open the file for reading.

        If you really need writing, you can use HashingWriter(). But please
        be aware that changing objects which are already in the cache may lead
        to unwanted results.
        """
        return open(self.local(), 'rb')

    def md5(self, value=None):
        """Return the md5sum of the file, or set it."""
        if value is not None:
            self._md5 = value
        elif self._md5 is None and self.is_local:
            self._md5 = apt_pkg.md5sum(self.open())
        elif self._md5 is None:
            self.local()
        return self._md5

    def sha1(self, value=None):
        """Return the sha1sum of the file, or set it."""
        if value is not None:
            self._sha1 = value
        elif self._sha1 is None and self.is_local:
            self._sha1 = apt_pkg.sha1sum(self.open())
        elif self._sha1 is None:
            self.local()
        return self._sha1

    def sha256(self, value=None):
        """Return the sha256sum of the file, or set it."""
        if value is not None:
            self._sha256 = value
        elif self._sha256 is None and self.is_local:
            self._sha256 = apt_pkg.sha256sum(self.open())
        elif self._sha256 is None:
            self.local()
        return self._sha256


class HashingWriter(object):
    """A file-like object for writing files into a pool.

    This uses hashlib to create the hashsums while writing the data to the
    file, which means we do not need to read the file anymore.

    The parameter 'pool' normally refers to a Pool() object. If fobj is given,
    it may also point to File() object.

    If the optional parameter 'fobj' is given, we update its File() object,
    instead of calling pool.add_file() to create a new file object. This
    facility is used in File.local() for example.
    """

    __slots__ = '_file', '_pool', 'md5', 'sha1', 'sha256', 'fobj'

    def __init__(self, pool, fobj=None):
        """See the class documentation"""
        self._pool = pool
        # pylint: disable-msg=W0212
        if fobj is not None and fobj._sha1 is not None:
            self._file = open(os.path.join(pool.path, fobj._sha1), 'wb')
        else:
            from tempfile import NamedTemporaryFile
            self._file = NamedTemporaryFile(dir=pool.path)
        self.fobj = fobj

        self.md5 = hashlib.md5() if not (fobj and fobj._md5) else None
        self.sha1 = hashlib.sha1() if not (fobj and fobj._sha1) else None
        self.sha256 = hashlib.sha256() if not (fobj and fobj._sha256) else None

    def __enter__(self):
        """Enter the context."""
        return self

    def __exit__(self, *_):
        """A simple context manager exit, which simply closes the file."""
        return self.close()

    def close(self):
        """Close the file."""
        if not self._file.closed:
            if self.sha1 is not None: # We have no sha1, need to rename it.
                target = os.path.join(self._pool.path, self.sha1.hexdigest())
                if not os.path.exists(target):
                    os.link(self._file.name, target)
                target = 'file:' + target
            else:
                target = self._file.name

            # Update the path to the local file.
            if self.fobj is None:
                self.fobj = self._pool.add_file(target, self._file.tell())
            else:
                self.fobj._local = target

            # Set the hashsums
            if self.md5 is not None:
                self.fobj.md5(self.md5.hexdigest())
            if self.sha1 is not None:
                self.fobj.sha1(self.sha1.hexdigest())
            if self.sha256 is not None:
                self.fobj.sha256(self.sha256.hexdigest())
        return self._file.close()

    def write(self, contents):
        """Write contents to the file."""
        self._file.write(contents)
        if self.md5 is not None:
            self.md5.update(contents)
        if self.sha1 is not None:
            self.sha1.update(contents)
        if self.sha256 is not None:
            self.sha256.update(contents)


class Pool(object):
    """The basic abstraction layer for file access.

    The pool keeps track of all the files, and should be the only part of
    debimg which actually deals with files, providing transparent access to
    multiple types of files.

    The pool has an additional mapping of sha1sums to File() objects, called
    the "cache". Objects are added to the cache if add_file() receives a 'sha1'
    parameter, if a package is added, or when compact() is called.

    The parameter 'tdir' defines the temporary directory at which files will
    be stored. It will be created automatically unless the parameter 'mkdir'
    is set to False.
    """

    __slots__ = ('path', '_files', '_cache')

    def __init__(self, tdir, mkdir=True):
        """See the class documentation"""
        self.path = tdir = os.path.abspath(tdir)
        self._files = []
        self._cache = {}

        if not os.path.exists(tdir):
            if mkdir:
                os.makedirs(tdir)
            else:
                raise ValueError('No directory %s, but mkdir=False' % tdir)
        else:
            for fname in os.listdir(tdir):
                # Load the already existing files from the pool dir.
                fpath = os.path.join(self.path, fname)
                if os.path.isfile(fpath):
                    fobj = File(self.path, fpath, sha1=fname)
                    self._files.append(fobj)
                    self._cache[fname] = fobj

    def add_package(self, package):
        """Add a new package to the pool.

        The parameter 'package' is a debimg.core.resolver.Package() object.
        """
        with package.record() as record:
            try:
                return self._cache[record.SHA1Hash]
            except KeyError:
                fobj = File(self.path, package.uri, package.size,
                            record.MD5Hash, record.SHA1Hash, record.SHA256Hash)
                self._cache[record.SHA1Hash] = fobj
                self._files.append(fobj)
                return fobj

    def add_file(self, source, size=None, md5=None, sha1=None, sha256=None):
        """Add a file to the pool, return File object.

        The parameters 'source', 'size' and 'md5' have the same meaning as in
        File().

        If 'sha1' is specified, and no object with this hashsum is available
        in the cache, it will be added. If it already exists in the cache, the
        existing object will be updated with the new source, and returned.
        """
        # pylint: disable-msg=R0913
        try:
            fobj = self._cache[sha1]
            fobj.sources.add(source)
            return fobj
        except KeyError:
            fobj = File(self.path, source, size, md5, sha1, sha256)
            self._files.append(fobj)
            if sha1 is not None:
                self._cache[sha1] = fobj
            return fobj

    def writing(self):
        """Return a HashingWriter() file-like object, for writing.

        See the documentation of HashingWriter() for further information.
        """
        return HashingWriter(self)

    def compact(self):
        """Compact the pool, ie. merge duplicate file objects.

        Normally, the pool does not keep track of equal files. By calling
        compact() the cache collects the SHA1 hashsums and removes all
        duplicate File objects.

        If you have a reference to a File object, you can keep it, because it
        will receive the same contents as the duplicates.
        """
        for fobj in self._files:
            try:
                self._cache[fobj.sha1()].sources.update(fobj.sources)
                fobj.sources = self._cache[fobj.sha1()].sources
                self._cache[fobj.sha1()]._local = fobj._local = None
            except KeyError:
                self._cache[fobj.sha1()] = fobj
        self._files = self._cache.values()

    def fetch(self):
        """Run the fetcher for all the files in the pool.

        This generates an instance of pycurl.Curl(), and calls the fetch()
        method of each pool member, passing the curl object.
        """
        curl = pycurl.Curl()
        curl.setopt(pycurl.NOPROGRESS, 0)
        curl.setopt(pycurl.FOLLOWLOCATION, 1)
        for fobj in self._files:
            if not fobj.is_local:
                fobj.fetch(curl)
        curl.close()
