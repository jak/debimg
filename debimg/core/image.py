# Copyright (C) 2009 Julian Andres Klode <jak@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Debimg Image Generation Part.

This is a preview release of the image generation code. Heavy changes may be
done in future versions.
"""
from __future__ import with_statement
from operator import attrgetter
import functools
import logging
import os
import shutil
import subprocess

from debimg.core.repository import Repository


def _proxy_to_all(cls, attribute):
    """Create a proxy for a given attribute."""

    @functools.wraps(getattr(cls, attribute))
    def func(self, *args, **kwds):
        """The basic wrapper function."""
        for member in self:
            ret = getattr(member, attribute)(*args, **kwds)
        else:
            return
        return ret

    return func


class Image(Repository):
    """Class for generating images.

    This class is a subclass of debimg.core.repository.Repository, extending it
    with support for size limits and alike.
    """
    __slots__ = 'sectors_used', 'sectors_total', 'disk_number'
    __sector_size = 2048

    def __init__(self, pool, base, size=0, number=1):
        Repository.__init__(self, pool, base)
        self.sectors_used = 0
        self.sectors_total = (size // self.__sector_size +
                              (size % self.__sector_size and 1))
        self.disk_number = number

    def use_sectors(self, size, check=True):
        """Check if a given size is free on the disk.

        If the requested space can be allocated, the number of sectors required
        is returned. Otherwise, a ValueError is raised.
        """
        sectors = size // self.__sector_size + (size % self.__sector_size and
                                                1)
        if not check or (sectors + self.sectors_used) < self.sectors_total:
            return sectors
        else:
            raise ValueError("Not enough space left on the image.")

    def add_group(self, group, distro=None):
        """Add a group of packages to the image."""
        try:
            if distro is None: # This always happens exactly once per group.
                self.sectors_used += self.use_sectors(sum(pkg.size for pkg in
                                                        group))
        except ValueError:
            raise ValueError('The group "%s" is too big for the image.' %
                             (group, ))
        return Repository.add_group(self, group, distro)

    def add_file(self, target, *args, **kwds):
        """Add a file to the image."""
        fileobj = Repository.add_file(self, target, *args, **kwds)
        try:
            self.sectors_used += self.use_sectors(fileobj.size())
        except ValueError:
            del self.files[target]
            raise ValueError('The file "%s" is too large.' % target)
        return fileobj

    def finalize_image(self, outfile, label, jigdo=False, info=True):
        """Finalize the image file.

        The parameter 'outfile' specifies the path the ISO image will be stored
        at.

        The paramter 'label' specifies the volume id of the image.

        The boolean parameter 'jigdo' specifies whether jigdo and template
        files should be created. If True, those files will be located in the
        same directory as the ISO image, with ".jigdo"/".template" appended to
        the filename. The default is False (and its not implemented yet).

        The boolean parameter 'info' specifies whether a file named
        ".disk/mkisofs" should be written to image, containing the command-line
        used when calling genisoimage. This is the default.

        debimg utilizes a genisoimage called 'graft points' in order to reduce
        the need for copying/linking files. Please do not use finalize_files()
        if you want to use 'graft points'.
        """

        logging.info('Creating disk %d in %s' % (self.disk_number, outfile))

        cmd = ['genisoimage', '-r', '-V', label, '-o', outfile, '-J',
               '-cache-inodes', '-q']

        outdir = os.path.dirname(outfile)
        if outfile != '/dev/null' and not os.path.exists(outdir):
            os.makedirs(outdir)

        if 'isolinux/isolinux.bin' in self.files:
            logging.info('Found isolinux/isolinux.bin, using it.')
            cmd.extend(('-b', 'isolinux/isolinux.bin', '-boot-load-size', '4',
                        '-c', 'isolinux/boot.cat', '-no-emul-boot',
                        '-boot-info-table'))

        cmd.append('-graft-points')
        cmd.append(self.path)

        for fname, fobj in self.files.iteritems():
            # Let's use graft points.
            if fobj.local().startswith(self.path):
                continue
            if os.path.exists(self.path + '/' + fname):
                continue
            if fname == 'isolinux/isolinux.bin':
                # Hack for isolinux.bin
                if not os.path.exists(self.path + '/isolinux'):
                    os.mkdir(self.path + '/isolinux')
                shutil.copyfile(fobj.local(), self.path + '/' + fname)
                continue
            fname = fname.replace('\\', '\\\\').replace('=', r'\=')
            local = fobj.local().replace('\\', '\\\\').replace('=', r'\=')
            cmd.append('%s=%s' % (fname, local))

        if info:
            if not os.path.exists(self.path + '/.disk'):
                os.mkdir(self.path + '/.disk')
            with open(self.path + '/.disk/mkisofs', 'w') as mkisofs:
                mkisofs.write(subprocess.list2cmdline(cmd))

        if subprocess.call(cmd) != 0:
            logging.error("The image %s could not be build." % outfile)


class MultipleImages(object):
    """Create multiple images.

    Please be aware that you lose flexibility when using this class, working
    with Distribution classes is not possible anymore for example.

    This shares the basic parts of the Image() API, and forwards function calls
    to all members.
    """

    def __init__(self, pool, base, size=0, max_disks=None):
        self._pool = pool
        self._size = size
        self._max = max_disks
        self._members = []
        self.path = base
        self.add()

    def __getitem__(self, item):
        return self._members[item]

    def __iter__(self):
        return iter(self._members)

    def add(self):
        """Add a new image to the collection."""
        count = len(self._members) + 1
        if self._max is not None and count > self._max:
            raise
        path = self.path % (count)
        image = Image(self._pool, path, self._size, count)
        self._members.append(image)
        return image

    def finalize_image(self, outfile, label, jigdo=False, info=True):
        """Call finalize_image() of all members.

        In contrast to Image.finalize_image(), the parameter 'outfile' needs to
        contain a '%d' which will be replaced by the disk number.
        """
        for image in self._members:
            image.finalize_image(outfile % image.disk_number, label,
                                 jigdo, info)

    def add_group(self, group, distro, force=True):
        """Add a given package group.

        In contrast to the single Image() version, this version requires the
        parameter 'distro' to be present because there is no way to work
        directly with Distribution() objects yet.

        First, this method tries to add the whole group to the current disk.
        If this fails, they will be added to a new disk (unless the maximum
        number of disks is reached).

        If the parameter 'force' is True (default), and both ways fail, the
        group is split. If not, the method raises a ValueError.
        """
        try:
            self._members[-1].add_group(group, distro)
        except ValueError:
            self.add()
            logging.info("Moving %s to next disk" % (group, ))
            try:
                self._members[-1].add_group(group, distro)
            except ValueError:
                if not force or len(group) == 1:
                    raise
                logging.warning("Splitting %s" % (group, ))
                del self._members[-1]
                for package in sorted(group, key=attrgetter('done')):
                    self.add_group((package, ), distro)

    finalize_contents = _proxy_to_all(Image, 'finalize_contents')
    finalize_files = _proxy_to_all(Image, 'finalize_files')
    finalize_hashsums = _proxy_to_all(Image, 'finalize_hashsums')
    finalize_packages = _proxy_to_all(Image, 'finalize_packages')
    finalize_release = _proxy_to_all(Image, 'finalize_release')
