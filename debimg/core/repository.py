# Copyright (C) 2009 Julian Andres Klode <jak@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Ignore "except Exception" warnings, and warnings about * and **
# pylint: disable-msg=W0142,W0703
"""Debian Repository management.

This provides two classes known as Distribution and Repository. The Repository
is responsible for all the files, and contains multiple Distribution objects
which are responsible for the packages.

The basic features are already finished, but it is still missing support for
source packages and size limitations, the latter being developed during the
creation of the image module.
"""
from __future__ import with_statement
from collections import defaultdict
import logging
import operator
import os
import subprocess
import time

import apt_inst
import apt_pkg

from debimg.core.compression import CompressedWriter

__all__ = 'Distribution', 'Repository'


class Distribution(object):
    """Manage Packages, Sources and Contents inside a distribution."""

    __slots__ = ('path', '_pfil', '_cont', 'codename', '_packages', '_parent')

    def __init__(self, parent, codename):
        """See the class documentation."""
        # Public API
        self.path = os.path.join(parent.path, 'dists', codename)
        self.codename = codename
        # Private API
        self._parent = parent
        self._pfil = os.path.join(self.path, '%s', 'binary-%s', 'Packages')
        self._packages = []

        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def relpath(self, path):
        """Return the relative path to the file."""
        if not path.startswith('/') and not path in self._parent.files:
            raise ValueError('Path does not start with "/": %s' % path)
        if not path.startswith(self.path):
            raise ValueError('Path not in %s: %s' % (self.path, path))
        return path[len(self._parent.path)+1:]

    def add_group(self, group):
        """Add a package group."""
        self._parent.add_group(group)
        for package in group:
            self._packages.append(package)

    def finalize_packages(self, gzip=True, bzip2=False):
        """Write the Packages files to the repository."""
        if not self._packages:
            return False
        sort_key = operator.attrgetter('arch', 'component', 'name')
        arch, component, fobj = None, None, None

        for package in sorted(self._packages, key=sort_key):
            if not (package.arch == arch and package.component == component):
                arch = package.arch
                component = package.component
                if fobj is not None:
                    fobj.close()
                    for fpath in fobj.names():
                        self._parent.add_file(self.relpath(fpath), fpath)
                fobj = CompressedWriter(self._pfil % (component, arch), True,
                                        gzip, bzip2, True)

            with package.record() as record:
                fobj.write(record.Record.strip())
                fobj.write("\n\n")
        fobj.close()
        for fpath in fobj.names():
            self._parent.add_file(self.relpath(fpath), fpath)

    def finalize_contents(self):
        """Write the Contents-ARCH.gz files to the repository."""
        contents = defaultdict(lambda: defaultdict(set))
        for package in self._packages:

            def callback(what, name, *_):
                """Called by debExtract."""
                if what == 'FILE':
                    contents[package.arch][name].add(package.fullname)

            with package.record() as record:
                with self._parent.files[record.FileName].open() as fobj:
                    for data in 'data.tar.gz', 'data.tar.bz2', 'data.tar.lzma':
                        try:
                            apt_inst.debExtract(fobj, callback, data)
                            break
                        except Exception, exc:
                            if data == 'data.tar.lzma':
                                logging.error(exc)
                                break
                            fobj.seek(0)

        for arch in contents:
            path = os.path.join(self.path, 'Contents-%s') % arch
            leng = max(len(fname) for fname in contents[arch])
            with CompressedWriter(path, False, True) as fobj:
                for fname, pkg in sorted(contents[arch].iteritems()):
                    fobj.write(fname)
                    # Approx. the same thing used in apt-ftparchive.
                    position = len(fname) - 8
                    fobj.write('\t')
                    position += 8 - position % 8
                    while leng - position > 0:
                        fobj.write('\t')
                        position += 8
                    #fobj.write((leng - position) * ' ')
                    fobj.write(','.join(pkg))
                    fobj.write('\n')

            for fpath in fobj.names():
                self._parent.add_file(self.relpath(fpath), fpath)

    def finalize_release(self, key=None, values=None, **extravalues):
        """Finalize the release files.

        TODO: Implement */binary-*/Release.

        Example::
            distro.finalize_release(Origin="DDD")
            distro.finalize_release(defaults=dict(Origin="DDD"))
        """
        archs = ' '.join(set(pkg.arch for pkg in self._packages))
        comps = ' '.join(set(pkg.component for pkg in self._packages))
        defaults = dict(Origin='Debian', Label='Debian', Architectures=archs,
                        Components=comps,
                        Date=apt_pkg.TimeRFC1123(int(time.time())),
                        Codename=self.codename)
        fields = ('Origin', 'Label', 'Codename', 'Suite', 'Version', 'Date',
                  'Valid-Until', 'Architectures', 'Components', 'Description')

        if values is not None:
            defaults.update(values)
        defaults.update(extravalues)

        with open(os.path.join(self.path, 'Release'), 'w') as release:
            for field in fields:
                value = defaults.get(field) or defaults.get(field.lower())
                if value:
                    release.write('%s: %s\n' % (field, value))

            hashes = defaultdict(list)
            for fname, fobj in self._parent.files.iteritems():
                if not fname.startswith(self.relpath(self.path)):
                    continue
                fname = fname[len(self.relpath(self.path))+1:]
                hashes['MD5Sum'].append((fobj.md5(), fobj.size(), fname))
                hashes['SHA1'].append((fobj.sha1(), fobj.size(), fname))
                hashes['SHA256'].append((fobj.sha256(), fobj.size(), fname))

            for name in 'MD5Sum', 'SHA1', 'SHA256':
                release.write('%s:\n' % name)
                for value in hashes[name]:
                    release.write(' %s %8s %s\n' % value)

        if key is not None:
            subprocess.check_call(("gpg", "--yes", "-abs", "-u" + key, "-o",
                                   "Release.gpg", "Release"), cwd=self.path)


class Repository(object):
    """A repository, actually a collection of multiple distributions."""

    __slots__ = 'path', '_dist', 'files', '_pool'

    def __init__(self, pool, base):
        """Please see the class documentation."""
        self._pool = pool
        self.path = os.path.abspath(base)
        self.files = {}
        self._dist = {}

        if not os.path.exists(base):
            os.makedirs(base)

    def add_distribution(self, codename):
        """Add a distribution object for the given codename and arch."""
        dist = self._dist[codename] = Distribution(self, codename)
        return dist

    def add_file(self, target, *args, **kwds):
        """Add a file to the disk."""
        fobj = self.files[target] = self._pool.add_file(*args, **kwds)
        return fobj

    def finalize_hashsums(self, hashtype='md5'):
        """Finalize hashsums files, eg. MD5SUMS.

        The parameter 'hashtype' may be one of "md5", "sha1", "sha256".
        """
        if hashtype != 'md5' and hashtype != 'sha1' and hashtype != 'sha256':
            raise ValueError('Wrong hashtype specified: "%s"' % hashtype)
        hasher = operator.attrgetter(hashtype)
        with open('%s/%sSUMS' % (self.path, hashtype.upper()), 'w') as writer:
            for fname, fobj in sorted(self.files.iteritems()):
                writer.write('%s  %s\n' % (hasher(fobj)(), fname))

    def finalize_files(self):
        """Finalize the files. Copy them to the target directory."""
        for fname, fobj in self.files.iteritems():
            fpath = os.path.join(self.path, fname)
            dpath = os.path.dirname(fpath)
            if not os.path.exists(dpath):
                os.makedirs(dpath)
            if not os.path.exists(fpath):
                try:
                    os.link(fobj.local(), fpath)
                except OSError, exc:
                    logging.error("Exception: %s, symlinking instead" % exc)
                    os.symlink(fobj.local(), fpath)

    def finalize_contents(self):
        """Write the Contents-ARCH.gz files to the repository."""
        for dist in self._dist.itervalues():
            dist.finalize_contents()

    def add_group(self, group, distro=None):
        """Add a package group to the repository."""
        if distro is not None:
            if not distro in self._dist:
                self.add_distribution(distro)
            return self._dist[distro].add_group(group)
        for package in group:
            with package.record() as record:
                self.files[record.FileName] = self._pool.add_package(package)

    def finalize_packages(self, gzip=True, bzip2=False):
        """Write the Packages files to the repository."""
        for dist in self._dist.itervalues():
            dist.finalize_packages(gzip, bzip2)

    def finalize_release(self, key=None, values=None, **extravalues):
        """Finalize the release files."""
        for dist in self._dist.itervalues():
            dist.finalize_release(key, values, **extravalues)
