# Copyright (C) 2009 Julian Andres Klode <jak@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Classes and functions related to compression."""
import os


class CompressedWriter(object):
    """Write to multiple files with compression using a single interface.

    CompressedWriter creates a file object for the given filename, and if
    requested, it also creates GzipFile and/or BZ2File objects, with '.gz' /
    '.bz2' appended to the filename.

    CompressedWriter supports a subset of the functionality of file objects,
    namely close,write,tell. It supports the with-statement.
    """

    __slots__ = ('_gobj', '_bobj', '_fobj')

    def __init__(self, fname, raw=True, gzip=False, bz2=False, mkdir=False):
        #pylint: disable-msg=R0913
        if not (raw or gzip or bz2):
            raise ValueError('At least one of raw, gzip, bz2 must be True')
        self._fobj = None
        self._gobj = None
        self._bobj = None
        if mkdir and not os.path.exists(os.path.dirname(fname)):
            os.makedirs(os.path.dirname(fname))
        if raw:
            self._fobj = open(fname, 'wb')
        if gzip:
            from gzip import GzipFile
            self._gobj = GzipFile(fname + '.gz', mode='wb')
        if bz2:
            from bz2 import BZ2File
            self._bobj = BZ2File(fname + '.bz2', mode='wb')

    def __enter__(self):
        return self

    def __exit__(self, *_):
        self.close()

    def tell(self):
        """Return the current file position."""
        if self._fobj is not None:
            return self._fobj.tell()
        if self._gobj is not None:
            return self._gobj.tell()
        if self._bobj is not None:
            return self._bobj.tell()

    def close(self):
        """Close the file object(s)."""
        if self._fobj is not None:
            self._fobj.close()
        if self._gobj is not None:
            self._gobj.close()
        if self._bobj is not None:
            self._bobj.close()

    def write(self, data):
        """Write data to the file(s)."""
        if self._fobj is not None:
            self._fobj.write(data)
        if self._gobj is not None:
            self._gobj.write(data)
        if self._bobj is not None:
            self._bobj.write(data)

    def names(self):
        """Iterator over all the file names."""
        if self._fobj is not None:
            yield self._fobj.name
        if self._gobj is not None:
            yield self._gobj.filename
        if self._bobj is not None:
            yield self._bobj.name


def deflate(data):
    """Deflate the given data.

    It can be decompressed using `zlib.decompress(data, -zlib.MAX_WBITS)`.
    """
    import zlib
    compressor = zlib.compressobj(9, zlib.DEFLATED, -zlib.MAX_WBITS)
    return compressor.compress(data) + compressor.flush()


def bzip2(data):
    """Compress data in one shot, using bzip2 and maximum compression."""
    import bz2
    return bz2.compress(data, 9)
